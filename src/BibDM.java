import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
public class BibDM{

    /**
     * Ajoute deux entiers
     * @param a le premier entier à ajouter
     * @param b le deuxieme entier à ajouter
     * @return la somme des deux entiers
     */
    public static Integer plus(Integer a, Integer b)
    {
        return a + b;
    }


    /**
     * Renvoie la valeur du plus petit élément d'une liste d'entiers
     * VOUS DEVEZ LA CODER SANS UTILISER COLLECTIONS.MIN (i.e. vous devez le faire avec un for)
     * @param liste
     * @return le plus petit élément de liste
     */
    public static Integer min(List<Integer> liste)
    {
        if(liste.isEmpty()){
            return null;
        }
        int min = liste.get(0);
        for(int i=1 ;i < liste.size(); i++){
            if (liste.get(i)<min){
                min=liste.get(i);
            }
        }
        return min;
    }


    /**
     * Teste si tous les élements d'une liste sont plus petits qu'une valeure donnée
     * @param valeur
     * @param liste
     * @return true si tous les elements de liste sont plus grands que valeur.
     */
    public static<T extends Comparable<? super T>> boolean plusPetitQueTous( T valeur , List<T> liste)
    {
        for(T elem : liste){
            if(elem.compareTo(valeur) < 1){
                return false;
            }
        }
        return true;
    }





    /**
     * Intersection de deux listes données par ordre croissant.
     * @param liste1 une liste triée
     * @param liste2 une liste triée
     * @return une liste triée avec les éléments communs à liste1 et liste2
     * */
    public static <T extends Comparable<? super T>> List<T> intersection(List<T> liste1, List<T> liste2){
        List<T> listeres = new ArrayList<>();
        if(liste1.equals(liste2) && !liste1.isEmpty()){
            T vprec = liste1.get(0);
            listeres.add(vprec);
            for(T elem : liste1){
                if (elem!= vprec){
                    listeres.add(elem);
                }
                vprec = elem;
            }
            return listeres;
        }
        int position_l1 = 0;
        int position_l2 = 0;
        while(position_l1 <= liste1.size() - 1 && position_l2 <= liste2.size() - 1){
            if((liste1.get(position_l1).compareTo(liste2.get(position_l2)))==0){
                if(!listeres.contains(liste1.get(position_l1))){
                    listeres.add(liste1.get(position_l1));
                }
                position_l1++;
                position_l2++;
            }
            else if((liste1.get(position_l1).compareTo(liste2.get(position_l2)))==1){
                position_l2++;
            }
            else{
                position_l1++;
            }
        }
        return listeres;
    }


    /**
     * Découpe un texte pour obtenir la liste des mots le composant. texte ne contient que des lettres de l'alphabet et des espaces.
     * @param texte une chaine de caractères
     * @return une liste de mots, correspondant aux mots de texte.
     */
    public static List<String> decoupe(String texte)
    {
        String mot = "";
        List<String> listeStr = new ArrayList<>();
        for (char caractere : texte.toCharArray()) {
            if(caractere == ' ' && !mot.equals("")){
                listeStr.add(mot);
                mot = "";
            }
            else if (caractere != ' '){
                mot += caractere;
            }
        }
        if(!mot.equals("")){
            listeStr.add(mot);
        }
        return listeStr;
    }


    /**
     * Renvoie le mot le plus présent dans un texte.
     * @param texte une chaine de caractères
     * @return le mot le plus présent dans le texte. En cas d'égalité, renvoyer le plus petit dans l'ordre alphabétique
     */

    public static String motMajoritaire(String texte)
    {
        if(texte.equals("")){
            return null;
        }
        HashMap<String, Integer> hashmots = new HashMap<>();
        List<String> listeTexte = decoupe(texte);
        for(String mot : listeTexte){
            if(hashmots.containsKey(mot)){
                hashmots.put(mot, hashmots.get(mot) + 1);
            }
            else{
                hashmots.put(mot, 1);
            }
        }
        String max = "";
        hashmots.put("", 0);
        for(String mot : hashmots.keySet()){
            if(hashmots.get(mot).equals(hashmots.get(max))){
                List<String> lisetStr = new ArrayList<>();
                lisetStr.add(mot);
                lisetStr.add(max);
                max = Collections.min(lisetStr);
            }
            if(hashmots.get(mot) > hashmots.get(max)){
                max = mot;
            }
        }
        return max;
    }

    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de ( et de )
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ()) est mal parenthèsée et (())() est bien parenthèsée.
     */
    public static boolean bienParenthesee(String chaine)
    {
        if(chaine.isEmpty()){
            return true;
        }
        int nb = 0;
        for (char caractere : chaine.toCharArray()){
            if(caractere != '('){
                nb -= 1;
            }
            if(caractere != ')'){
                nb += 1;
            }
            if(nb < 0){
                return false;
            }
        }
        return nb == 0;
    }

    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de (, de  ), de [ et de ]
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ([)] est mal parenthèsée alors ue ([]) est bien parenthèsée.
     */
    public static boolean bienParentheseeCrochets(String chaine)
    {
        int crochetDroit = 0;
        int crochetGauche = 0;
        int parGauche = 0;
        int parDroit = 0;
        boolean res = true;
        int i = 0;
        if(chaine.isEmpty()){
            return true;
        }
        else if(chaine.length() == 1){
            return false;
        }
        else {
            while(i < chaine.length() && res){
                if(chaine.charAt(i)=='('){
                    parGauche += 1;
                    if(i == chaine.length() - 1 || (i < chaine.length() - 1 && chaine.charAt(i + 1) == ']')){
                        res = false;
                    }
                }
                else if(chaine.charAt(i)==')'){
                    parDroit += 1;
                    if(i == 0 || (i > chaine.length() - 1 && chaine.charAt(i - 1) == '[')){
                        res = false;
                    }
                }
                else if(chaine.charAt(i)=='['){
                    crochetGauche += 1;
                    if(i == chaine.length() - 1 || (i < chaine.length() - 1 && chaine.charAt(i + 1) == ')')){
                        res = false;
                    }
                }
                else if(chaine.charAt(i)==']'){
                    crochetDroit += 1;
                    if(i == 0 || (i > chaine.length() - 1 && chaine.charAt(i - 1) == '(')){
                        res = false;
                    }
                }
                i += 1;
            }
            if(parGauche != parDroit || crochetGauche != crochetDroit){
                res = false;
            }
        }
        return res;
    }


    /**
     * Recherche par dichtomie d'un élément dans une liste triée
     * @param liste, une liste triée d'entiers
     * @param valeur un entier
     * @return true si l'entier appartient à la liste.
     */
    public static boolean rechercheDichotomique(List<Integer> liste, Integer valeur)
    {
        if (liste.size() > 0){
            int centre;
            int haut = liste.size();
            int bas = 0;

            while (bas < haut){
                centre = ((bas + haut) / 2);
                if(valeur.compareTo(liste.get(centre)) == 0){
                    return true;
                }
                else if(valeur.compareTo(liste.get(centre)) > 0){
                    bas = centre + 1;
                }
                else{
                    haut = centre;
                }
            }
        }
        return false;
    }
}
